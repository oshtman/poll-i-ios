//
//  Poll.swift
//  Poll-i
//
//  Created by Andreas Östman on 2018-08-31.
//  Copyright © 2018 Poll-i. All rights reserved.
//

import Foundation

struct Poll: Codable {
    let id: String
    let shareUrl: String
    let users: [User]
    let answers: [String: Int]
}
