//
//  User.swift
//  Poll-i
//
//  Created by Andreas Östman on 2018-08-31.
//  Copyright © 2018 Poll-i. All rights reserved.
//

import Foundation

struct User: Codable {
    let id: String
    var name: String?
    var answer: String?
}
