//
//  ViewController.swift
//  Poll-i
//
//  Created by Andreas Östman on 2018-08-30.
//  Copyright © 2018 Poll-i. All rights reserved.
//

import UIKit
import SocketIO

class ViewController: UIViewController, AllPollsViewControllerDelegate {
    
    @IBOutlet weak var votedLabel: UILabel!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    private var options: [UIButton]?
    private let allPollsIdentifier = "allPollsSegue"
    private var poll: Poll? {
        didSet {
            if(options == nil) { setInitialOptions() }
            votedLabel.text = poll == nil ? "" : "\(poll!.users.filter{ $0.answer != nil }.count) / \(poll!.users.count) have voted"
            createButton.isHidden = poll != nil
            poll?.answers.forEach{ (key,value) in
                let option = options?.first(where: { String(($0.titleLabel?.text?.split(separator: "\n").first)!) == key })
                option?.setTitle(key + "\n(\(value) votes)", for: .normal)
            }
        }
    }
    
    @IBAction func createPoll(_ sender: Any) {
        handler.send(event: .create)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handler.listen(to: .create) { [weak self] in
            self?.poll = $0
        }
        handler.listen(to: .join(pollId: nil)) { [weak self] in
            self?.poll = $0
        }
        handler.listen(to: .vote(value: nil)) { [weak self] in
            self?.poll = $0
        }
        handler.listen(to: .update) { [weak self] in
            self?.poll = $0
        }
    }
    
    private func setInitialOptions() {
        self.options = self.poll?.answers.keys.map{
            let button = UIButton()
            button.titleLabel?.numberOfLines = 3
            button.titleLabel?.textAlignment = .center
            button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
            button.setTitle($0 + "\n(0 votes)", for: .normal)
            button.setTitleColor(.blue, for: .normal)
            button.layer.borderColor = UIColor.blue.cgColor
            button.layer.borderWidth = 2.0
            button.addTarget(self, action: #selector(self.vote(_:)), for: .touchUpInside)
            return button
        }
        self.options?.forEach{ self.stackView.addArrangedSubview($0) }
    }
    
    func didSelectPoll(_ poll: Poll) {
        handler.send(event: .join(pollId: poll.id))
    }
    
    @objc func vote(_ button: UIButton) {
        options?.forEach{ $0.backgroundColor = .white }
        button.backgroundColor = UIColor.gray.withAlphaComponent(0.2)
        let keys = Array(poll!.answers.keys)
        let index = options!.index(where: { $0 == button })!
        handler.send(event: .vote(value: keys[index]))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case .some(allPollsIdentifier):
            let destination = segue.destination as? AllPollsViewController
            destination?.delegate = self
        default:
            break
        }
    }
}
