//
//  AppDelegate.swift
//  Poll-i
//
//  Created by Andreas Östman on 2018-08-30.
//  Copyright © 2018 Poll-i. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

