//
//  Sockets.swift
//  Poll-i
//
//  Created by Andreas Östman on 2018-08-31.
//  Copyright © 2018 Poll-i. All rights reserved.
//

import UIKit
import SocketIO

let endpoint = "http://localhost:3000"

enum Event {
    case create
    case join(pollId: String?)
    case vote(value: String?)
    case listAll
    case update

    var endpoint: String {
        switch self {
        case .create:
            return "poll:create"
        case .join:
            return "poll:join"
        case .vote:
            return "poll:vote"
        case .listAll:
            return "polls:all"
        case .update:
            return "poll:update"
        }
    }
}

let handler = SocketHandler()

class SocketHandler {
    
    typealias JSON = [String: Any]
    
    let manager: SocketManager
    let socket: SocketIOClient?
    
    init() {
        manager = SocketManager(socketURL: URL(string: endpoint)!,
                                config: [.log(true), .compress])
        socket = manager.defaultSocket
        socket?.connect()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(appBecameActive),
                                               name: .UIApplicationDidBecomeActive,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(appBecameInactive),
                                               name: .UIApplicationDidEnterBackground,
                                               object: nil)
    }
    
    @objc private func appBecameActive() {
        guard socket?.status.active == false else { return }
        socket?.connect()
    }
    
    @objc private func appBecameInactive() {
        guard socket?.status.active == true else { return }
        socket?.disconnect()
    }
    
    func listen<T: Codable>(to event: Event, on: @escaping (T) -> ()) {
        socket?.on(event.endpoint, callback: { (data, _) in
            print("Recieved event: ", event.endpoint, "with data: ", data)
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            guard let jsonData = try? JSONSerialization.data(withJSONObject: data.first!, options: .prettyPrinted) else { return }
            let object = try! decoder.decode(T.self, from: jsonData)
            on(object)
        })
    }
    
    func send(event: Event) {
        var data: SocketData?
        switch event {
        case .join(let pollId):
            data = pollId
        case .vote(let value):
            data = value
        default:
            data = []
        }
        socket?.emit(event.endpoint, data!)
    }
}
