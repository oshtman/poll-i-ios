//
//  AllPollsViewController.swift
//  Poll-i
//
//  Created by Andreas Östman on 2018-09-02.
//  Copyright © 2018 Poll-i. All rights reserved.
//

import UIKit

protocol AllPollsViewControllerDelegate: class {
    func didSelectPoll(_ poll: Poll)
}

class AllPollsViewController: UITableViewController {
    
    weak var delegate: AllPollsViewControllerDelegate?
    private var polls: [Poll] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    private let cellIdentifier = "Cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsMultipleSelection = false
        handler.listen(to: .listAll) { [weak self] (polls: [Poll]) in
            self?.polls = polls
        }
        handler.send(event: .listAll)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return polls.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) else { return UITableViewCell() }
        let poll = polls[indexPath.row]
        cell.textLabel?.text = "Poll \(poll.id)"
        cell.detailTextLabel?.text = "\(poll.users.count) participants"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let poll = polls[indexPath.row]
        delegate?.didSelectPoll(poll)
        navigationController?.popViewController(animated: true)
    }
    
}
